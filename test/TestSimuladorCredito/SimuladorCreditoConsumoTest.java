/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSimuladorCredito;

/*import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;*/
import org.junit.Before;
import static org.junit.Assert.*;
import models.Banco;
import models.CreditoConsumo;
import models.ValidarCreditoConsumo;
import org.junit.Test;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;


/**
 *
 * @author richar
 */
public class SimuladorCreditoConsumoTest {
    public Errors errors;
    public ValidarCreditoConsumo validadorConsumo;
    
    @Before public void setUp() {
        validadorConsumo = new ValidarCreditoConsumo();
        
    }    
    @Test
    public void SimularCuotaTest(){
        CreditoConsumo credito = new CreditoConsumo();
        Banco banco = new Banco();
        credito.setMontoCredito("2000000");
        credito.setPlazoCredito("23");
        credito.setMesesGracia(0);
        //prueba
        int resultado = banco.simularCuotaMensual(credito);
        int esperado = 109933;
        assertEquals(esperado,resultado);
    }
    @Test
    public void MesesGraciaTest(){
        CreditoConsumo credito = new CreditoConsumo();
        credito.setMesesGracia(2);
        errors = new BeanPropertyBindingResult(credito, "credito");
        validadorConsumo.validate(credito, errors);
        assertFalse(errors.hasFieldErrors("mesesGracia"));
    }
    @Test
    public void MontoCreditoTest(){
        CreditoConsumo credito = new CreditoConsumo();
        credito.setMontoCredito("dasd");
        errors = new BeanPropertyBindingResult(credito, "credito");
        validadorConsumo.validate(credito, errors);
        assertTrue(errors.hasFieldErrors("montoCredito"));
    }
    @Test
    public void RunTest(){
        CreditoConsumo credito = new CreditoConsumo();
        credito.setRun("23.450.653-2");
        errors = new BeanPropertyBindingResult(credito, "credito");
        validadorConsumo.validate(credito, errors);
        assertFalse(errors.hasFieldErrors("run"));
    }
    @Test
    public void PlazoCreditoTest(){
        CreditoConsumo credito = new CreditoConsumo();
        credito.setPlazoCredito("69");
        errors = new BeanPropertyBindingResult(credito,"credito");
        validadorConsumo.validate(credito, errors);
        assertTrue(errors.hasFieldErrors("plazoCredito"));
    }
    
    
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
