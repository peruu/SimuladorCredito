/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author richar
 */

public class CreditoConsumo {
    private String run;
    private String montoCredito;
    private String plazoCredito;
    private int mesesGracia;
    private String tipoCredito;
    public static double interesMensual=0.0205;
    public static double interesAnual=0.246;
    public static int notario=1500;
    public CreditoConsumo(){
        this.run = "";
        this.montoCredito = "";
        this.plazoCredito = "";
        this.mesesGracia = 0;
    }
    /*public CreditoConsumo(String run, String montoCredito, String plazoCredito, int mesesGracia) {
        this.run = run;
        this.montoCredito = montoCredito;
        this.plazoCredito = plazoCredito;
        this.mesesGracia = mesesGracia;
    }*/
    
    public String getRun() {
        return run;
    }

    public void setRun(String run) {
        this.run = run;
    }

    public String getMontoCredito() {
        return montoCredito;
    }

    public void setMontoCredito(String montoCredito) {
        this.montoCredito = montoCredito;
    }

    public String getPlazoCredito() {
        return plazoCredito;
    }

    public void setPlazoCredito(String plazoCredito) {
        this.plazoCredito = plazoCredito;
    }

    public int getMesesGracia() {
        return mesesGracia;
    }

    public void setMesesGracia(int mesesGracia) {
        this.mesesGracia = mesesGracia;
    }

    public String getTipoCredito() {
        
        return tipoCredito;
    }

    public void setTipoCredito(String tipoCredito) {
        this.tipoCredito = tipoCredito;
    }
  
    
}
