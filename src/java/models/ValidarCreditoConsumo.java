/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author richar
 */
public class ValidarCreditoConsumo implements Validator  {
    private static final String RUN_PATTERN = "\\d{1,2}\\.?\\d{3}\\.?\\d{3}\\-?[0-9kK]{1}";
    private static final String SOLO_DIGITOS_PATTERN = "^\\d*\\d$"; 
    
    
    private Pattern pattern;
    private Matcher matcher;
    @Override
    public boolean supports(Class<?> type) {
        return CreditoConsumo.class.isAssignableFrom(type);
    }
    @Override
    public void validate(Object o, Errors errors) {
        CreditoConsumo credito =(CreditoConsumo)o;
        validarMontoCredito(credito.getMontoCredito(),errors);
        validarPlazoCredito(credito.getPlazoCredito(),errors);
        validarFormatoRun(credito.getRun(),errors);
        validarMesesGracia(credito.getMesesGracia(),errors);
        //Validar que los campos no esten vacios
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "run","required.run",
                "El campo run es obligatorio");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "plazoCredito","required.plazoCredito",
                "El campo plazo credito es obligatorio");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "montoCredito","required.montoCredito",
                "El campo monto credito es obligatorio");
        
    }
    private void validarMesesGracia(int mesesGracia,Errors errors){
        if(mesesGracia!=0 && mesesGracia!=1 && mesesGracia!=2 && mesesGracia!=3){
            errors.rejectValue("mesesGracia", "invalid.mesesGracia",
                    "Meses gracia solo puedes ser 0,1,2 o 3");
        }
    }
    private void validarMontoCredito(String monto,Errors errors){
        if(!(monto != null && monto.isEmpty())){
            this.pattern = Pattern.compile(SOLO_DIGITOS_PATTERN);
            this.matcher = pattern.matcher(monto);
            if(!matcher.matches()){
                errors.rejectValue("montoCredito","invalid.monto","Ingrese un monto correcto");
            }else{
                try{
                    int montoCredito = Integer.parseInt(monto);
                    if(montoCredito<1500000 || montoCredito>20000000){
                        errors.rejectValue("montoCredito", "invalid.montoCredito",
                        "El monto credito minimo debe ser 1500000 y maximo 20000000");
                    }
                }catch(NumberFormatException ex){ 
                        errors.rejectValue("montoCredito", "invalid.montoCredito",
                        "El monto credito minimo debe ser 1500000 y maximo 20000000");
                }
            }
        }
        
    }
    private void validarPlazoCredito(String plazo,Errors errors){
        if(!(plazo != null && plazo.isEmpty())){
            this.pattern = Pattern.compile(SOLO_DIGITOS_PATTERN);
            this.matcher = pattern.matcher(plazo);
            if(!matcher.matches()){
                errors.rejectValue("plazoCredito","invalid.plazo","Ingrese un plazo de credito correcto");
            }else{
                try{
                    int plazoCredito = Integer.parseInt(plazo);
                    if(plazoCredito<12 || plazoCredito>36){
                        errors.rejectValue("plazoCredito", "invalid.plazoCredito",
                        "EL plazo credito minimo debe ser 12 y maximo 36 ");
                    }
                }catch(NumberFormatException ex){
                    errors.rejectValue("plazoCredito", "invalid.plazoCredito",
                    "EL plazo credito minimo debe ser 12 y maximo 36 ");
                }
            }
        }
        
    }
    private void validarFormatoRun(String run,Errors errors){
        if(!(run != null && run.isEmpty())){
            this.pattern = Pattern.compile(RUN_PATTERN);
            this.matcher = pattern.matcher(run);
            if(!matcher.matches()){
                errors.rejectValue("run","invalid.run","El run "+run+
                      " ingresado no es valido.");
            }
        }
    }


   
    
}
