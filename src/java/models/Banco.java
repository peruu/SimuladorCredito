/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author richar
 */
public class Banco {
    
    
    public int simularCuotaMensual(CreditoConsumo credito){
        //i = impuesto,m = mesesGracia,n= plazoCredito,c= montoCredito
        //a = c*i*[(i+1)^(n-m)]
        //b = (1+i)^m
        //c = ((i+1)^(n-m))-1
        //cuota = (a*b)/c
        int montoCredito = Integer.parseInt(credito.getMontoCredito());
        int plazoCredito = Integer.parseInt(credito.getPlazoCredito());
        int mesesGracia = credito.getMesesGracia();
        double impuesto = CreditoConsumo.interesMensual;
        double a = montoCredito*impuesto*Math.pow(impuesto+1,plazoCredito-mesesGracia);
        double b = Math.pow(1+impuesto,mesesGracia);
        double c = Math.pow(1+impuesto,plazoCredito-mesesGracia)-1;
        double cuota = (a*b)/c;
        return (int) cuota;
    }
}
