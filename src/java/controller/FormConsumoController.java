/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import models.Banco;
import models.CreditoConsumo;
import models.ValidarCreditoConsumo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author richar
 */
@Controller
@RequestMapping(value="index.htm")
public class FormConsumoController {
    
    @RequestMapping(value="",method=RequestMethod.GET)
    public String formConsumos(){
        return "";
    }
    @RequestMapping(method=RequestMethod.GET)
    public ModelAndView formConsumoGet(){
        //muestra la pagina en blanco
        ModelAndView mav = new ModelAndView("index","command",new CreditoConsumo());
        mav.addObject("creditoConsumo",new CreditoConsumo());
        return mav;
    }
    @RequestMapping(method=RequestMethod.POST)
    public ModelAndView formConsumoPost(@ModelAttribute("creditoConsumo") CreditoConsumo credito,
                              BindingResult result,SessionStatus status){
        ModelAndView mav = new ModelAndView();
        ValidarCreditoConsumo validarConsumo = new  ValidarCreditoConsumo();
        validarConsumo.validate(credito, result);
        if ( result.hasErrors() ) {
            mav.setViewName("index"); // tiene errores
            return mav;
	}else{// si no tiene errores 
            Banco banco = new Banco();
            double cuota = banco.simularCuotaMensual(credito);
            mav.setViewName("resultadoConsumo"); 
            mav.addObject("run", credito.getRun());
            mav.addObject("montoCredito", credito.getMontoCredito());
            mav.addObject("plazoCredito", credito.getPlazoCredito());
            mav.addObject("mesesGracia", credito.getMesesGracia());
            mav.addObject("tipoCredito", credito.getTipoCredito());
            mav.addObject("cuota",cuota);
            mav.addObject("interesMensual",CreditoConsumo.interesMensual*100);
            mav.addObject("interesAnual",CreditoConsumo.interesAnual*100);
            mav.addObject("notario",CreditoConsumo.notario);
            mav.addObject("costoTotal",CreditoConsumo.notario+Integer.parseInt(credito.getMontoCredito()));
            return mav;
        }
        
        
    }
} 
