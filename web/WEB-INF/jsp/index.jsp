<%-- 
    Document   : index
    Created on : 11-09-2018, 12:52:23
    Author     : richar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <title>Credito consumo</title>
    </head>
    <body>
        <header class="container-fluid" >
            <h1 class="text-center">Simulador Credito consumo</h1>
        </header>
        <div class="container">
        <div class="row">
        <div class="col-6 align-self-center">
        <form:form method="post" commandName="creditoConsumo">
            <form:errors path="*" element="div" cssClass="alert alert-danger"/> 
            
            <form:label path="run" >RUN</form:label>
            <form:input path="run" cssClass="form-control" type="text" maxlength="12" />
            <small class="form-text text-muted">Ingrese rut con puntos y guion</small>
            
            <form:label path="plazoCredito">Plazo Credito</form:label>
            <form:input path="plazoCredito" cssClass="form-control"  
                         />
            
            <form:label path="montoCredito">Monto Credito</form:label>
            <form:input path="montoCredito" cssClass="form-control"  
                        />   
            
            <form:label path="tipoCredito">Tipo Credito</form:label>
            <form:select path="tipoCredito" cssClass="form-control">
            <form:option value="Credito Estandar">Crédito Estándar</form:option>
            <form:option value="Consumo Universal">Consumo universal</form:option>
            </form:select>
            
            <form:label path="mesesGracia">Meses gracia</form:label>
            <form:select path="mesesGracia" cssClass="form-control">
            <form:option value="0">0</form:option>
            <form:option value="1">1</form:option>
            <form:option value="2">2</form:option>
            <form:option value="3">3</form:option>
            </form:select> 
                        
            <input type="submit" value="Simular" class="btn btn-primary"/>
        </form:form>
        </div>
        </div>
        </div>
    </body>
</html>
