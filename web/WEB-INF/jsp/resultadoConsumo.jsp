<%-- 
    Document   : home
    Created on : 11-09-2018, 2:05:26
    Author     : richar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <title>Credito sonsumo</title>
    </head>
    <body>
        <header class="container-fluid" >
            <h1 class="text-center">Simulador credito consumo</h1>
        </header>
        <div class="container">
        <div class="row">
            <div class="col-6 ">
                <h3 class="text">Datos del credito</h3>
                <table class="table">
                <tbody>
                  <tr>
                    <th>Fecha de simulacion</th>
                    <td>Hoy</td>
                  </tr>
                  <tr>
                    <th>Tipo de credito</th>
                    <td>${tipoCredito}</td>
                  </tr>
                  <tr>
                    <th>Moneda</th>
                    <td>Pesos chilenos</td>
                  </tr>
                  <tr>
                    <th>Monto del credito solicitado</th>
                    <td>${montoCredito}</td>
                  </tr>
                  <tr>
                    <th>Plazo del crédito</th>
                    <td>${plazoCredito}</td>
                  </tr>
                  <tr>
                    <th>Meses de Gracia</th>
                    <td>${mesesGracia}</td>
                  </tr>
                </tbody>
              </table>
            </div>
            
            <div class="col-6 ">
                <h3 class="text">Resultados de la simulacion</h3>
                <table class="table">
                <tbody>
                  <tr>
                    <th>Valor cuota mensual</th>
                    <td>${cuota}</td>
                  </tr>
                  <tr>
                    <th>Tasa de interes mensual</th>
                    <td>${interesMensual}</td>
                  </tr>
                  <tr>
                    <th>Tasa de interes anual</th>
                    <td>${interesAnual}</td>
                  </tr>
                  <tr>
                    <th>Notario</th>
                    <td>${notario}</td>
                  </tr>
                  <tr>
                    <th>Costo Total del Credito</th>
                    <td>${costoTotal}</td>
                  </tr>
                </tbody>
              </table>
            </div>
        </div>
        </div>
    </body>
</html>
